const request = require('supertest')
const app = require('../server')

describe('GET/Tampil/Ayah', () => {
  it('Tampil data ayah', async () => {
    const res = await request(app)
      .get('/ayah/tampil');
      //console.log(res.body);
      expect(res.status).toEqual(200);
      expect(res.body.value).toEqual(res.body.value);
  });
});

describe('POST/Tambah/Ayah', () => {
  it('Tambah data ayah', async () => {
    const res = await request(app)
      .post('/ayah/tambah')
      .send({
        'kode_ayah':'AY0005',
        'nama_ayah':'Coba',
        'nik_ayah':'3210087687676222',
        'tahun_lahir_ayah':'1969',
        'kode_pendidikan':'10',
        'kode_pekerjaan':'05',
        'kode_penghasilan':'5',
        'kode_kebutuhan_khusus':'01'
      });
      //console.log(res.body);
      expect(res.status).toEqual(200);
      expect(res.body.values).toEqual('Data Berhasil Ditambahkan');
  });
});

describe('PUT/Ubah/Ayah', () => {
  it('Ubah data ayah', async () => {
    const res = await request(app)
      .put('/ayah/ubah')
      .send({
        'kode_ayah':'AY0005',
        'nama_ayah':'Coba Nama Ayah',
        'nik_ayah':'3210087687676222',
        'tahun_lahir_ayah':'1969',
        'kode_pendidikan':'10',
        'kode_pekerjaan':'05',
        'kode_penghasilan':'5',
        'kode_kebutuhan_khusus':'01'
      });
      //console.log(res.body);
      expect(res.status).toEqual(200);
      expect(res.body.values).toEqual('Data Berhasil Diubah');
  });
});

describe('DELETE/Hapus/Ayah', () => {
  it('Hapus data ayah', async () => {
    const res = await request(app)
      .delete(`/ayah/hapus/`)
      .send({
        'kode_ayah':'AY0005'
      });
      console.log(res.body);
      expect(res.status).toEqual(200);
      expect(res.body.values).toEqual('Data Berhasil Dihapus');
  });
});

//=======================================================================test ibu
describe('GET/Tampil/Ibu', () => {
  it('Tampil data ibu', async () => {
    const res = await request(app)
      .get('/ibu/tampil');
      //console.log(res.body);
      expect(res.status).toEqual(200);
      expect(res.body.value).toEqual(res.body.value);
  });
});

describe('POST/Tambah/Ibu', () => {
  it('Tambah data ibu', async () => {
    const res = await request(app)
      .post('/ibu/tambah')
      .send({
        'kode_ibu':'IB005',
        'nama_ibu':'Ibu Test',
        'nik_ibu':'3453425362111',
        'tahun_lahir_ibu':'1972',
        'kode_pendidikan':'09',
        'kode_pekerjaan':'08',
        'kode_penghasilan':'5',
        'kode_kebutuhan_khusus':'01'
      });
      //console.log(res.body);
      expect(res.status).toEqual(200);
      expect(res.body.values).toEqual('Data Berhasil Ditambahkan');
  });
});

describe('PUT/Ubah/Ibu', () => {
  it('Ubah data ibu', async () => {
    const res = await request(app)
      .put('/ibu/ubah')
      .send({
        'kode_ibu':'IB005',
        'nama_ibu':'Ibu Test 2',
        'nik_ibu':'3453425362111',
        'tahun_lahir_ibu':'1972',
        'kode_pendidikan':'09',
        'kode_pekerjaan':'08',
        'kode_penghasilan':'5',
        'kode_kebutuhan_khusus':'01'
      });
      //console.log(res.body);
      expect(res.status).toEqual(200);
      expect(res.body.values).toEqual('Data Berhasil Diubah');
  });
});

describe('DELETE/Hapus/Ibu', () => {
  it('Hapus data ibu', async () => {
    const res = await request(app)
      .delete('/ibu/hapus/')
      .send({
        'kode_ibu':'IB005'
      });
      //console.log(res.body);
      expect(res.status).toEqual(200);
      expect(res.body.values).toEqual('Data Berhasil Dihapus');
  });
});

//=======================================================================test pekerjaan
describe('GET/Tampil/Pekerjaan', () => {
  it('Tampil data pekerjaan', async () => {
    const res = await request(app)
      .get('/pekerjaan/tampil');
      //console.log(res.body);
      expect(res.status).toEqual(200);
      expect(res.body.value).toEqual(res.body.value);
  });
});

describe('POST/Tambah/Pekerjaan', () => {
  it('Tambah data pekerjaan', async () => {
    const res = await request(app)
      .post('/pekerjaan/tambah')
      .send({
        'kode_pekerjaan':'12',
        'nama_pekerjaan':'Pensiun'
      });
      //console.log(res.body);
      expect(res.status).toEqual(200);
      expect(res.body.values).toEqual('Data Berhasil Ditambahkan');
  });
});

describe('PUT/Ubah/Pekerjaan', () => {
  it('Ubah data pekerjaan', async () => {
    const res = await request(app)
      .put('/pekerjaan/ubah')
      .send({
        'kode_pekerjaan':'12',
        'nama_pekerjaan':'Pensiunan'
      });
      //console.log(res.body);
      expect(res.status).toEqual(200);
      expect(res.body.values).toEqual('Data Berhasil Diubah');
  });
});

describe('DELETE/Hapus/Pekerjaan', () => {
  it('Hapus data pekerjaan', async () => {
    const res = await request(app)
      .delete('/pekerjaan/hapus/')
      .send({
        'kode_pekerjaan':'12'
      });
      //console.log(res.body);
      expect(res.status).toEqual(200);
      expect(res.body.values).toEqual('Data Berhasil Dihapus');
  });
});

//=======================================================================test pendidikan
describe('GET/Tampil/Pendidikan', () => {
  it('Tampil data pendidikan', async () => {
    const res = await request(app)
      .get('/pendidikan/tampil');
      //console.log(res.body);
      expect(res.status).toEqual(200);
      expect(res.body.value).toEqual(res.body.value);
  });
});

describe('GET/Tampil/Pendidikan', () => {
  it('Tampil data pendidikan by id', async () => {
    const res = await request(app)
      .get(`/pendidikan/edit/07`);
      //console.log(res.body);
      expect(res.status).toEqual(200);
      expect(res.body.value).toEqual(res.body.value);
  });
});

describe('POST/Tambah/Pendidikan', () => {
  it('Tambah data pendidikan', async () => {
    const res = await request(app)
      .post('/pendidikan/tambah')
      .send({
        'kode_pendidikan':'07',
        'nama_pendidikan':'D1'
      });
      //console.log(res.body);
      expect(res.status).toEqual(200);
      expect(res.body.values).toEqual('Data Berhasil Ditambahkan');
  });
});

describe('PUT/Ubah/Pendidikan', () => {
  it('Ubah data pendidikan', async () => {
    const res = await request(app)
      .put('/pendidikan/ubah')
      .send({
        'kode_pendidikan':'07',
        'nama_pendidikan':'D2'
      });
      //console.log(res.body);
      expect(res.status).toEqual(200);
      expect(res.body.values).toEqual('Data Berhasil Diubah');
  });
});

describe('DELETE/Hapus/Pendidikan', () => {
  it('Hapus data pendidikan', async () => {
    const res = await request(app)
      .delete('/pendidikan/hapus/')
      .send({
        'kode_pendidikan':'07'
      });
      //console.log(res.body);
      expect(res.status).toEqual(200);
      expect(res.body.values).toEqual('Data Berhasil Dihapus');
  });
});

describe('DELETE/Hapus/Pendidikan', () => {
  it('Hapus data pendidikan', async () => {
    const kode_pendidikan = '11';
    const res = await request(app)
      .delete(`/pendidikan/hapus/${kode_pendidikan}`);
      //console.log(res.body);
      expect(res.status).toEqual(200);
      expect(res.body.values).toEqual('Data Berhasil Dihapus');
  });
});

describe('POST/Tambah/Pendidikan', () => {
  it('Tambah data pendidikan', async () => {
    const res = await request(app)
      .post('/pendidikan/tambah')
      .send({
        'kode_pendidikan':'11',
        'nama_pendidikan':'S3'
      });
      //console.log(res.body);
      expect(res.status).toEqual(200);
      expect(res.body.values).toEqual('Data Berhasil Ditambahkan');
  });
});

//=======================================================================test Wali
describe('GET/Tampil/Wali', () => {
  it('Tampil data wali', async () => {
    const res = await request(app)
      .get('/wali/tampil');
      //console.log(res.body);
      expect(res.status).toEqual(200);
      expect(res.body.value).toEqual(res.body.value);
  });
});

describe('POST/Tambah/Wali', () => {
  it('Tambah data wali', async () => {
    const res = await request(app)
      .post('/wali/tambah')
      .send({
        'kode_wali':'W005',
        'nama_wali':'Test Wali',
        'nik_wali':'3455465465467111',
        'tahun_lahir_wali':'1970',
        'kode_pendidikan':'01',
        'kode_pekerjaan':'01',
        'kode_penghasilan':'1'
      });
      //console.log(res.body);
      expect(res.status).toEqual(200);
      expect(res.body.values).toEqual('Data Berhasil Ditambahkan');
  });
});

describe('PUT/Ubah/Wali', () => {
  it('Ubah data wali', async () => {
    const res = await request(app)
      .put('/wali/ubah')
      .send({
        'kode_wali':'W005',
        'nama_wali':'Test Wali 2',
        'nik_wali':'3455465465467000',
        'tahun_lahir_wali':'1970',
        'kode_pendidikan':'01',
        'kode_pekerjaan':'01',
        'kode_penghasilan':'1'
      });
      //console.log(res.body);
      expect(res.status).toEqual(200);
      expect(res.body.values).toEqual('Data Berhasil Diubah');
  });
});

describe('DELETE/Hapus/Wali', () => {
  it('Hapus data wali', async () => {
    const res = await request(app)
      .delete('/wali/hapus/')
      .send({
        'kode_wali':'W005'
      });
      //console.log(res.body);
      expect(res.status).toEqual(200);
      expect(res.body.values).toEqual('Data Berhasil Dihapus');
  });
});

//=======================================================================test Siswa
describe('GET/Tampil/Siswa', () => {
  it('Tampil data siswa', async () => {
    const res = await request(app)
      .get('/siswa/tampil');
      //console.log(res.body);
      expect(res.status).toEqual(200);
      expect(res.body.value).toEqual(res.body.value);
  });
});

describe('POST/Tambah/Siswa', () => {
  it('Tambah data siswa', async () => {
    const res = await request(app)
      .post('/siswa/tambah')
      .send({
        'nik':'328173536472101',
        'nama_lengkap':'Ardi Rinaldi',
        'jenis_kelamin':'Laki-Laki',
        'nisn':'6736262111',
        'no_kitas':'0',
        'tempat_lahir':'Bandung',
        'tanggal_lahir':'2015-03-06',
        'agama':'01',
        'kewarganegaraan':'Indonesia (WNI)',
        'nama_negara':'Indonesia',
        'berkebutuhan_khusus':'01',
        'kode_alamat':'AL003',
        'alamat_jalan':'Jl. Simpang 4 No.53',
        'rt':'003',
        'rw':'001',
        'tempat_tinggal':'1',
        'mode_transportasi':'01',
        'no_kps':'678',
        'no_kip':'0878111',
        'bank':'BNI',
        'nomor_regis_akta_lahir':'87261111',
        'no_rek_bank':'62090111',
        'nama_rekening':'Sumireh',
        'kode_ayah':'AY0003',
        'kode_ibu':'IB003',
        'kode_wali':'W003',
        'tinggi_badan':'171',
        'berat_badan':'60',
        'jarak_ke_sekolah':'Kurang dari 1 km',
        'sebutkan_jarak':'200',
        'waktu_tempuh_jam':'1',
        'waktu_tempuh_menit':'10',
        'jumlah_saudara_kandung':'2',
        'kode_prestasi':'PR003',
        'kode_beasiswa':'BE003',
        'kode_regis':'RG003',
        'nomor_peserta_ujian':'UJ003',
        'kode_keluar':'KL003'
      });
      //console.log(res.body);
      expect(res.status).toEqual(200);
      expect(res.body.values).toEqual('Data Berhasil Ditambahkan');
  });
});

describe('PUT/Ubah/Siswa', () => {
  it('Ubah data siswa', async () => {
    const res = await request(app)
      .put('/siswa/ubah')
      .send({
        'nik':'328173536472101',
        'nama_lengkap':'Ardi Rina',
        'jenis_kelamin':'Laki-Laki',
        'nisn':'6736262111',
        'no_kitas':'0',
        'tempat_lahir':'Bandung',
        'tanggal_lahir':'2015-03-06',
        'agama':'01',
        'kewarganegaraan':'Indonesia (WNI)',
        'nama_negara':'Indonesia',
        'berkebutuhan_khusus':'01',
        'kode_alamat':'AL003',
        'alamat_jalan':'Jl. Simpang 4 No.53',
        'rt':'003',
        'rw':'001',
        'tempat_tinggal':'1',
        'mode_transportasi':'01',
        'no_kps':'678',
        'no_kip':'0878111',
        'bank':'BNI',
        'nomor_regis_akta_lahir':'87261111',
        'no_rek_bank':'62090111',
        'nama_rekening':'Sumireh',
        'kode_ayah':'AY0003',
        'kode_ibu':'IB003',
        'kode_wali':'W003',
        'tinggi_badan':'171',
        'berat_badan':'60',
        'jarak_ke_sekolah':'Kurang dari 1 km',
        'sebutkan_jarak':'200',
        'waktu_tempuh_jam':'1',
        'waktu_tempuh_menit':'10',
        'jumlah_saudara_kandung':'2',
        'kode_prestasi':'PR003',
        'kode_beasiswa':'BE003',
        'kode_regis':'RG003',
        'nomor_peserta_ujian':'UJ003',
        'kode_keluar':'KL003'
      });
      //console.log(res.body);
      expect(res.status).toEqual(200);
      expect(res.body.values).toEqual('Data Berhasil Diubah');
  });
});

describe('DELETE/Hapus/Siswa', () => {
  it('Hapus data siswa', async () => {
    const res = await request(app)
      .delete('/siswa/hapus/')
      .send({
        'nik':'328173536472101'
      });
      //console.log(res.body);
      expect(res.status).toEqual(200);
      expect(res.body.values).toEqual('Data Berhasil Dihapus');
  });
});