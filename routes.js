'use strict';
    const controllerSiswa = require('./controller/controllerSiswa');
    const controllerAyah = require('./controller/controllerAyah');
    const controllerIbu = require('./controller/controllerIbu');
    const controllerWali = require('./controller/controllerWali');
    const controllerPendidikan = require('./controller/controllerPendidikan');
    const controllerPekerjaan = require('./controller/controllerPekerjaan');

module.exports = function(app){
    //siswa
    app.route('/siswa/tampil').get(controllerSiswa.tampil);
    app.route('/siswa/tambah').post(controllerSiswa.tambah);
    app.route('/siswa/ubah').put(controllerSiswa.ubah);
    app.route('/siswa/hapus').delete(controllerSiswa.hapus);

    //Ayah
    app.route('/ayah/tampil').get(controllerAyah.tampil);
    app.route('/ayah/tambah').post(controllerAyah.tambah);
    app.route('/ayah/ubah').put(controllerAyah.ubah);
    app.route('/ayah/hapus').delete(controllerAyah.hapus);

    //Ibu
    app.route('/ibu/tampil').get(controllerIbu.tampil);
    app.route('/ibu/tambah').post(controllerIbu.tambah);
    app.route('/ibu/ubah').put(controllerIbu.ubah);
    app.route('/ibu/hapus').delete(controllerIbu.hapus);

    //Wali
    app.route('/wali/tampil').get(controllerWali.tampil);
    app.route('/wali/tambah').post(controllerWali.tambah);
    app.route('/wali/ubah').put(controllerWali.ubah);
    app.route('/wali/hapus').delete(controllerWali.hapus);

    //pendidikan
    app.route('/pendidikan/tampil').get(controllerPendidikan.viewPendidikan);
    app.route('/pendidikan/edit/:id').get(controllerPendidikan.getPendidikanById);
    app.route('/pendidikan/tambah').post(controllerPendidikan.addPendidikan);
    app.route('/pendidikan/ubah').put(controllerPendidikan.updatePendidikan);
    app.route('/pendidikan/hapus').delete(controllerPendidikan.deletePendidikan);
    app.route('/pendidikan/hapus/:id').delete(controllerPendidikan.deletePendidikanId);

    //Pekerjaan
    app.route('/pekerjaan/tampil').get(controllerPekerjaan.tampil);
    app.route('/pekerjaan/tambah').post(controllerPekerjaan.tambah);
    app.route('/pekerjaan/ubah').put(controllerPekerjaan.ubah);
    app.route('/pekerjaan/hapus').delete(controllerPekerjaan.hapus);  
    
}