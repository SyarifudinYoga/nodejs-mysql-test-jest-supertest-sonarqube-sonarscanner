-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 10 Feb 2021 pada 03.11
-- Versi server: 10.1.38-MariaDB
-- Versi PHP: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pesertadidik`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_penghasilan`
--

CREATE TABLE `tbl_penghasilan` (
  `kode_penghasilan` enum('1','2','3','4','5','6') NOT NULL,
  `penghasilan_bulanan` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_penghasilan`
--

INSERT INTO `tbl_penghasilan` (`kode_penghasilan`, `penghasilan_bulanan`) VALUES
('1', '<  Rp.500.000'),
('2', 'Rp.500.000 - Rp.999.999'),
('3', 'Rp.1 Jt - Rp. 1.9 Jt'),
('4', 'Rp.2 Jt - Rp. 4.9 Jt'),
('5', 'Rp.5 Jt - Rp. 20 Jt'),
('6', '> =Rp. 21 Jt');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_wali`
--

CREATE TABLE `tbl_wali` (
  `kode_wali` varchar(7) NOT NULL,
  `nama_wali` varchar(30) NOT NULL,
  `nik_wali` varchar(16) NOT NULL,
  `tahun_lahir_wali` year(4) NOT NULL,
  `kode_pendidikan` enum('01','02','03','04','05','06','07','08','09','10','11') NOT NULL,
  `kode_pekerjaan` enum('01','02','03','04','05','06','07','08','09','10','11','12','99') NOT NULL,
  `kode_penghasilan` enum('1','2','3','4','5','6') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_wali`
--

INSERT INTO `tbl_wali` (`kode_wali`, `nama_wali`, `nik_wali`, `tahun_lahir_wali`, `kode_pendidikan`, `kode_pekerjaan`, `kode_penghasilan`) VALUES
('W001', 'Enjang', '3455465465467625', 1978, '04', '02', '1'),
('W002', 'Endi R', '3455465465467826', 1977, '05', '03', '2'),
('W003', 'Rumanah Arbani', '3455465465467111', 1970, '01', '01', '1');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_alamat`
--

CREATE TABLE `tb_alamat` (
  `kode_alamat` varchar(7) NOT NULL,
  `nama_dusun` enum('Cibeber','Cibeureum','Leuwi Gajah','Melong','Utama Cimahi') NOT NULL,
  `nama_kelurahan` enum('Cibeber','Cibeureum','Leuwi Gajah','Melong','Utama Cimahi') NOT NULL,
  `kecamatan` enum('Cimahi Selatan','Cimahi Utara') NOT NULL,
  `kode_pos` enum('40513','40514') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_alamat`
--

INSERT INTO `tb_alamat` (`kode_alamat`, `nama_dusun`, `nama_kelurahan`, `kecamatan`, `kode_pos`) VALUES
('AL001', 'Cibeber', 'Cibeber', 'Cimahi Utara', '40513'),
('AL002', 'Cibeber', 'Cibeber', 'Cimahi Selatan', '40514'),
('AL003', 'Cibeureum', 'Cibeureum', 'Cimahi Utara', '40513'),
('AL004', 'Leuwi Gajah', 'Leuwi Gajah', 'Cimahi Selatan', '40513'),
('AL005', 'Leuwi Gajah', 'Leuwi Gajah', 'Cimahi Utara', '40513'),
('AL006', 'Melong', 'Melong', 'Cimahi Selatan', '40513'),
('AL007', 'Cibeureum', 'Cibeureum', 'Cimahi Selatan', '40513'),
('AL008', 'Cibeureum', 'Cibeureum', 'Cimahi Selatan', '40514'),
('AL009', 'Utama Cimahi', 'Utama Cimahi', 'Cimahi Selatan', '40513');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_ayah`
--

CREATE TABLE `tb_ayah` (
  `kode_ayah` varchar(7) NOT NULL,
  `nama_ayah` varchar(30) NOT NULL,
  `nik_ayah` varchar(16) NOT NULL,
  `tahun_lahir_ayah` year(4) NOT NULL,
  `kode_pendidikan` enum('01','02','03','04','05','06','07','08','09','10','11') NOT NULL,
  `kode_pekerjaan` enum('01','02','03','04','05','06','07','08','09','10','11','12','99') NOT NULL,
  `kode_penghasilan` enum('1','2','3','4','5','6') NOT NULL,
  `kode_kebutuhan_khusus` enum('01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_ayah`
--

INSERT INTO `tb_ayah` (`kode_ayah`, `nama_ayah`, `nik_ayah`, `tahun_lahir_ayah`, `kode_pendidikan`, `kode_pekerjaan`, `kode_penghasilan`, `kode_kebutuhan_khusus`) VALUES
('AY0001', 'Budi', '3210087687676528', 1987, '08', '04', '2', '01'),
('AY0002', 'Munandar', '3210087687676382', 1977, '03', '02', '1', '01'),
('AY0003', 'Rafi Jaelani', '3210087687676222', 1969, '10', '05', '5', '01');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_beasiswa`
--

CREATE TABLE `tb_beasiswa` (
  `kode_beasiswa` varchar(7) NOT NULL,
  `jenis_beasiswa` enum('1','2','3','4','99') NOT NULL,
  `keterangan_beasiswa` varchar(30) NOT NULL,
  `tahun_mulai` year(4) NOT NULL,
  `tahun_selesai` year(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_beasiswa`
--

INSERT INTO `tb_beasiswa` (`kode_beasiswa`, `jenis_beasiswa`, `keterangan_beasiswa`, `tahun_mulai`, `tahun_selesai`) VALUES
('BE001', '1', 'Anak Berprestasi', 2018, 2020),
('BE002', '2', 'Anak Miskin', 2018, 2020),
('BE003', '3', 'Pendidikan', 2018, 2020),
('BE004', '4', 'Unggulan', 2018, 2020),
('BE005', '99', 'Lainnya', 2018, 2020);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_data_diri`
--

CREATE TABLE `tb_data_diri` (
  `nik` varchar(16) NOT NULL,
  `nama_lengkap` varchar(30) NOT NULL,
  `jenis_kelamin` enum('Laki-Laki','Perempuan') NOT NULL,
  `nisn` varchar(10) NOT NULL,
  `no_kitas` varchar(20) NOT NULL,
  `tempat_lahir` varchar(30) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `agama` enum('01','02','03','04','05','06','99') NOT NULL,
  `kewarganegaraan` enum('Indonesia (WNI)','Asing (WNA') NOT NULL,
  `nama_negara` varchar(20) NOT NULL,
  `berkebutuhan_khusus` enum('01','02','03','04','05','06','07','08','09','10','11','12','13','15','16','17','18') NOT NULL,
  `kode_alamat` varchar(7) NOT NULL,
  `alamat_jalan` text NOT NULL,
  `rt` varchar(3) NOT NULL,
  `rw` varchar(3) NOT NULL,
  `tempat_tinggal` enum('1','2','3','4','5','9') NOT NULL,
  `mode_transportasi` enum('01','02','03','04','05','06','07','08','99') NOT NULL,
  `no_kps` varchar(7) NOT NULL,
  `no_kip` varchar(7) NOT NULL,
  `bank` varchar(10) NOT NULL,
  `nomor_regis_akta_lahir` varchar(20) NOT NULL,
  `no_rek_bank` varchar(20) NOT NULL,
  `nama_rekening` varchar(30) NOT NULL,
  `kode_ayah` varchar(7) NOT NULL,
  `kode_ibu` varchar(7) NOT NULL,
  `kode_wali` varchar(7) NOT NULL,
  `tinggi_badan` smallint(6) NOT NULL,
  `berat_badan` float NOT NULL,
  `jarak_ke_sekolah` enum('Kurang dari 1 km','Lebih dari 1 km') NOT NULL,
  `sebutkan_jarak` smallint(6) NOT NULL,
  `waktu_tempuh_jam` smallint(6) NOT NULL,
  `waktu_tempuh_menit` smallint(6) NOT NULL,
  `jumlah_saudara_kandung` smallint(6) NOT NULL,
  `kode_prestasi` varchar(7) NOT NULL,
  `kode_beasiswa` varchar(7) NOT NULL,
  `kode_regis` varchar(7) NOT NULL,
  `nomor_peserta_ujian` varchar(20) NOT NULL,
  `kode_keluar` varchar(7) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_data_diri`
--

INSERT INTO `tb_data_diri` (`nik`, `nama_lengkap`, `jenis_kelamin`, `nisn`, `no_kitas`, `tempat_lahir`, `tanggal_lahir`, `agama`, `kewarganegaraan`, `nama_negara`, `berkebutuhan_khusus`, `kode_alamat`, `alamat_jalan`, `rt`, `rw`, `tempat_tinggal`, `mode_transportasi`, `no_kps`, `no_kip`, `bank`, `nomor_regis_akta_lahir`, `no_rek_bank`, `nama_rekening`, `kode_ayah`, `kode_ibu`, `kode_wali`, `tinggi_badan`, `berat_badan`, `jarak_ke_sekolah`, `sebutkan_jarak`, `waktu_tempuh_jam`, `waktu_tempuh_menit`, `jumlah_saudara_kandung`, `kode_prestasi`, `kode_beasiswa`, `kode_regis`, `nomor_peserta_ujian`, `kode_keluar`) VALUES
('3210263773638271', 'Umar Bakri', 'Laki-Laki', '6736262619', '-', 'Bandung', '2013-02-02', '01', 'Indonesia (WNI)', 'Indonesia', '01', 'AL001', 'Jl.Margahayu, Nomor 20.', '002', '001', '1', '01', '123', '0878651', 'BNI', '87261728', '62090918', 'Amizah', 'AY0001', 'IB001', 'W001', 141, 42, 'Kurang dari 1 km', 200, 1, 10, 4, 'PR001', 'BE001', 'RG001', 'UJ001', 'KL001'),
('328173536472111', 'Ardi Rinaldi', 'Laki-Laki', '6736262111', '-', 'Bandung', '2015-03-06', '01', 'Indonesia (WNI)', 'Indonesia', '01', 'AL003', 'Jl. Simpang 4 No.53', '003', '001', '1', '01', '678', '0878111', 'BRI', '87261111', '62090111', 'Sumireh N', 'AY0003', 'IB003', 'W003', 150, 50, 'Kurang dari 1 km', 200, 1, 10, 2, 'PR003', 'BE003', 'RG003', 'UJ003', 'KL003'),
('328173536472837', 'Ahsan Husen', 'Laki-Laki', '6736262617', '-', 'Sumedang', '2015-01-06', '01', 'Indonesia (WNI)', 'Indonesia', '01', 'AL002', 'Hl. Margondah 34', '003', '001', '1', '01', '345', '0878791', 'BRI', '87261123', '62090123', 'Rudiana', 'AY0002', 'IB002', 'W002', 141, 32, 'Lebih dari 1 km', 2000, 1, 20, 2, 'PR002', 'BE002', 'RG002', 'UJ002', 'KL002');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_ibu`
--

CREATE TABLE `tb_ibu` (
  `kode_ibu` varchar(7) NOT NULL,
  `nama_ibu` varchar(30) NOT NULL,
  `nik_ibu` varchar(16) NOT NULL,
  `tahun_lahir_ibu` year(4) NOT NULL,
  `kode_pendidikan` enum('01','02','03','04','05','06','07','08','09','10','11') NOT NULL,
  `kode_pekerjaan` enum('01','02','03','04','05','06','07','08','09','10','11','12','99') NOT NULL,
  `kode_penghasilan` enum('1','2','3','4','5','6') NOT NULL,
  `kode_kebutuhan_khusus` enum('01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_ibu`
--

INSERT INTO `tb_ibu` (`kode_ibu`, `nama_ibu`, `nik_ibu`, `tahun_lahir_ibu`, `kode_pendidikan`, `kode_pekerjaan`, `kode_penghasilan`, `kode_kebutuhan_khusus`) VALUES
('IB001', 'Santi', '3453425362435', 1973, '06', '01', '1', '01'),
('IB002', 'Risna', '3453425362888', 1975, '03', '02', '2', '01'),
('IB003', 'Feni Levia', '3453425362111', 1972, '09', '08', '5', '01');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_kebutuhan_khusus`
--

CREATE TABLE `tb_kebutuhan_khusus` (
  `kode_kebutuhan_khusus` enum('01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18') NOT NULL,
  `berkebutuhan_khusus` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_kebutuhan_khusus`
--

INSERT INTO `tb_kebutuhan_khusus` (`kode_kebutuhan_khusus`, `berkebutuhan_khusus`) VALUES
('01', 'Tidak'),
('02', 'Netra (A)'),
('03', 'Rungu (B)'),
('04', 'Grahita Ringan (C)'),
('05', 'Grahita Sedang (C1)'),
('06', 'Daksa Ringan (D1)'),
('07', 'Daksa Sedang (D1)'),
('08', 'Lanas (E)'),
('09', 'Wicara (F)'),
('10', 'Tuna Ganda (G)'),
('11', 'Hiper Aktif (H)'),
('12', 'Cerdas Istimewa'),
('13', 'Bakat Istimewa (j)'),
('14', 'Kesulitan Belajar (K'),
('15', 'Narkoba (N)'),
('16', 'Indigo (O)'),
('17', 'Down Syndrome (P)'),
('18', 'Autis/Mental (Q)');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_keluar`
--

CREATE TABLE `tb_keluar` (
  `kode_keluar` varchar(7) NOT NULL,
  `keluar_karena` enum('1','2','3','4','5','6','7','8') NOT NULL,
  `tanggal_keluar` date NOT NULL,
  `alasan_keluar` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_keluar`
--

INSERT INTO `tb_keluar` (`kode_keluar`, `keluar_karena`, `tanggal_keluar`, `alasan_keluar`) VALUES
('KL001', '1', '2021-02-08', 'Pindah Rumah'),
('KL002', '2', '2021-02-08', 'Pindah Rumah'),
('KL003', '1', '2021-02-09', 'Lulus Akselerasi');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_kip`
--

CREATE TABLE `tb_kip` (
  `nomor_kip` varchar(7) NOT NULL,
  `nama_tertera_kip` varchar(30) NOT NULL,
  `nomor_kks` varchar(7) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_kip`
--

INSERT INTO `tb_kip` (`nomor_kip`, `nama_tertera_kip`, `nomor_kks`) VALUES
('0878111', 'Airin Nur', '6547891'),
('0878651', 'Rudi Sapta', '7654321'),
('0878791', 'Amar Nahi', '1234567');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_kps`
--

CREATE TABLE `tb_kps` (
  `no_kps` varchar(7) NOT NULL,
  `layak_pip` enum('ya','tidak') NOT NULL,
  `alasan_layak` enum('01','02','03','04','05','06','07','08','09') NOT NULL,
  `penerima_kps` enum('ya','tidak') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_kps`
--

INSERT INTO `tb_kps` (`no_kps`, `layak_pip`, `alasan_layak`, `penerima_kps`) VALUES
('123', 'ya', '01', 'ya'),
('345', 'ya', '02', 'ya'),
('678', 'ya', '01', 'tidak');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_pekerjaan`
--

CREATE TABLE `tb_pekerjaan` (
  `kode_pekerjaan` enum('01','02','03','04','05','06','07','08','09','10','11','12','99') NOT NULL,
  `nama_pekerjaan` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_pekerjaan`
--

INSERT INTO `tb_pekerjaan` (`kode_pekerjaan`, `nama_pekerjaan`) VALUES
('01', 'Tidak Bekerja'),
('02', 'Nelayan'),
('03', 'Petani'),
('04', 'Peternak'),
('05', 'PNS/TNI/POLRI'),
('06', 'Karyawan Swasta'),
('07', 'Pedagang Kecil'),
('08', 'Pedagang Besar'),
('09', 'Wiraswasta'),
('10', 'Wirausaha'),
('11', 'Buruh'),
('12', 'Pensiunan'),
('99', 'Lainnya');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_pendidikan`
--

CREATE TABLE `tb_pendidikan` (
  `kode_pendidikan` enum('1','2','3','4','5','6','7','8','9','10','11') NOT NULL,
  `nama_pendidikan` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_pendidikan`
--

INSERT INTO `tb_pendidikan` (`kode_pendidikan`, `nama_pendidikan`) VALUES
('1', 'Tidak Sekolah'),
('2', 'Putus SD'),
('3', 'SD Sederajat'),
('4', 'SMP Sederajat'),
('5', 'SMA Sederajat'),
('6', 'D1'),
('7', 'D2'),
('8', 'D3'),
('9', 'D4/S1'),
('10', 'S2'),
('11', 'S3 Doktor');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_prestasi`
--

CREATE TABLE `tb_prestasi` (
  `kode_prestasi` varchar(7) NOT NULL,
  `jenis_prestasi` enum('1','2','3','4') NOT NULL,
  `tingkat_prestasi` enum('1','2','3','4','5','6') NOT NULL,
  `nama_prestasi` varchar(30) NOT NULL,
  `tahun_prestasi` year(4) NOT NULL,
  `penyelenggara` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_prestasi`
--

INSERT INTO `tb_prestasi` (`kode_prestasi`, `jenis_prestasi`, `tingkat_prestasi`, `nama_prestasi`, `tahun_prestasi`, `penyelenggara`) VALUES
('PR001', '1', '2', 'Olimpiade MIPA', 2014, 'Disdik'),
('PR002', '3', '5', 'Pekan Olahraga Nasional', 2015, 'Kemenpora'),
('PR003', '3', '6', 'Sepakbola Asia Tenggara', 2018, 'FIFA');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_regis`
--

CREATE TABLE `tb_regis` (
  `kode_regis` varchar(7) NOT NULL,
  `jenis_pendaftaran` enum('1','2') NOT NULL,
  `tanggal_masuk_sekolah` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_regis`
--

INSERT INTO `tb_regis` (`kode_regis`, `jenis_pendaftaran`, `tanggal_masuk_sekolah`) VALUES
('RG001', '1', '2021-02-08'),
('RG002', '2', '2021-02-08'),
('RG003', '1', '2021-02-10');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_ujian`
--

CREATE TABLE `tb_ujian` (
  `nomor_peserta_ujian` varchar(10) NOT NULL,
  `no_seri_ijazah` varchar(20) NOT NULL,
  `no_seri_skhus` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_ujian`
--

INSERT INTO `tb_ujian` (`nomor_peserta_ujian`, `no_seri_ijazah`, `no_seri_skhus`) VALUES
('UJ001', '789187263728', '625367281716'),
('UJ002', '789187263612', '625367281887'),
('UJ003', '789187263000', '625367281000');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `tbl_penghasilan`
--
ALTER TABLE `tbl_penghasilan`
  ADD PRIMARY KEY (`kode_penghasilan`);

--
-- Indeks untuk tabel `tbl_wali`
--
ALTER TABLE `tbl_wali`
  ADD PRIMARY KEY (`kode_wali`),
  ADD KEY `wli1` (`kode_pendidikan`),
  ADD KEY `wli2` (`kode_pekerjaan`),
  ADD KEY `wli3` (`kode_penghasilan`);

--
-- Indeks untuk tabel `tb_alamat`
--
ALTER TABLE `tb_alamat`
  ADD PRIMARY KEY (`kode_alamat`);

--
-- Indeks untuk tabel `tb_ayah`
--
ALTER TABLE `tb_ayah`
  ADD PRIMARY KEY (`kode_ayah`),
  ADD KEY `ayh1` (`kode_pendidikan`),
  ADD KEY `ayh2` (`kode_pekerjaan`),
  ADD KEY `ayh3` (`kode_penghasilan`),
  ADD KEY `ayh4` (`kode_kebutuhan_khusus`);

--
-- Indeks untuk tabel `tb_beasiswa`
--
ALTER TABLE `tb_beasiswa`
  ADD PRIMARY KEY (`kode_beasiswa`);

--
-- Indeks untuk tabel `tb_data_diri`
--
ALTER TABLE `tb_data_diri`
  ADD PRIMARY KEY (`nik`),
  ADD KEY `ddr1` (`kode_alamat`),
  ADD KEY `ddr2` (`kode_ayah`),
  ADD KEY `kps1` (`no_kps`),
  ADD KEY `kip1` (`no_kip`),
  ADD KEY `kib1` (`kode_ibu`),
  ADD KEY `kow1` (`kode_wali`),
  ADD KEY `kpr1` (`kode_prestasi`),
  ADD KEY `kbs1` (`kode_beasiswa`),
  ADD KEY `krg1` (`kode_regis`),
  ADD KEY `npu1` (`nomor_peserta_ujian`),
  ADD KEY `kkl1` (`kode_keluar`);

--
-- Indeks untuk tabel `tb_ibu`
--
ALTER TABLE `tb_ibu`
  ADD PRIMARY KEY (`kode_ibu`),
  ADD KEY `ibu1` (`kode_pendidikan`),
  ADD KEY `ibu2` (`kode_pekerjaan`),
  ADD KEY `ibu3` (`kode_penghasilan`),
  ADD KEY `ibu4` (`kode_kebutuhan_khusus`);

--
-- Indeks untuk tabel `tb_kebutuhan_khusus`
--
ALTER TABLE `tb_kebutuhan_khusus`
  ADD PRIMARY KEY (`kode_kebutuhan_khusus`);

--
-- Indeks untuk tabel `tb_keluar`
--
ALTER TABLE `tb_keluar`
  ADD PRIMARY KEY (`kode_keluar`);

--
-- Indeks untuk tabel `tb_kip`
--
ALTER TABLE `tb_kip`
  ADD PRIMARY KEY (`nomor_kip`);

--
-- Indeks untuk tabel `tb_kps`
--
ALTER TABLE `tb_kps`
  ADD PRIMARY KEY (`no_kps`);

--
-- Indeks untuk tabel `tb_pekerjaan`
--
ALTER TABLE `tb_pekerjaan`
  ADD PRIMARY KEY (`kode_pekerjaan`);

--
-- Indeks untuk tabel `tb_pendidikan`
--
ALTER TABLE `tb_pendidikan`
  ADD PRIMARY KEY (`kode_pendidikan`);

--
-- Indeks untuk tabel `tb_prestasi`
--
ALTER TABLE `tb_prestasi`
  ADD PRIMARY KEY (`kode_prestasi`);

--
-- Indeks untuk tabel `tb_regis`
--
ALTER TABLE `tb_regis`
  ADD PRIMARY KEY (`kode_regis`);

--
-- Indeks untuk tabel `tb_ujian`
--
ALTER TABLE `tb_ujian`
  ADD PRIMARY KEY (`nomor_peserta_ujian`);

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `tbl_wali`
--
ALTER TABLE `tbl_wali`
  ADD CONSTRAINT `wli1` FOREIGN KEY (`kode_pendidikan`) REFERENCES `tb_pendidikan` (`kode_pendidikan`),
  ADD CONSTRAINT `wli2` FOREIGN KEY (`kode_pekerjaan`) REFERENCES `tb_pekerjaan` (`kode_pekerjaan`),
  ADD CONSTRAINT `wli3` FOREIGN KEY (`kode_penghasilan`) REFERENCES `tbl_penghasilan` (`kode_penghasilan`);

--
-- Ketidakleluasaan untuk tabel `tb_ayah`
--
ALTER TABLE `tb_ayah`
  ADD CONSTRAINT `ayh1` FOREIGN KEY (`kode_pendidikan`) REFERENCES `tb_pendidikan` (`kode_pendidikan`),
  ADD CONSTRAINT `ayh2` FOREIGN KEY (`kode_pekerjaan`) REFERENCES `tb_pekerjaan` (`kode_pekerjaan`),
  ADD CONSTRAINT `ayh3` FOREIGN KEY (`kode_penghasilan`) REFERENCES `tbl_penghasilan` (`kode_penghasilan`),
  ADD CONSTRAINT `ayh4` FOREIGN KEY (`kode_kebutuhan_khusus`) REFERENCES `tb_kebutuhan_khusus` (`kode_kebutuhan_khusus`);

--
-- Ketidakleluasaan untuk tabel `tb_data_diri`
--
ALTER TABLE `tb_data_diri`
  ADD CONSTRAINT `ddr1` FOREIGN KEY (`kode_alamat`) REFERENCES `tb_alamat` (`kode_alamat`),
  ADD CONSTRAINT `ddr2` FOREIGN KEY (`kode_ayah`) REFERENCES `tb_ayah` (`kode_ayah`),
  ADD CONSTRAINT `kbs1` FOREIGN KEY (`kode_beasiswa`) REFERENCES `tb_beasiswa` (`kode_beasiswa`),
  ADD CONSTRAINT `kib1` FOREIGN KEY (`kode_ibu`) REFERENCES `tb_ibu` (`kode_ibu`),
  ADD CONSTRAINT `kip1` FOREIGN KEY (`no_kip`) REFERENCES `tb_kip` (`nomor_kip`),
  ADD CONSTRAINT `kkl1` FOREIGN KEY (`kode_keluar`) REFERENCES `tb_keluar` (`kode_keluar`),
  ADD CONSTRAINT `kow1` FOREIGN KEY (`kode_wali`) REFERENCES `tbl_wali` (`kode_wali`),
  ADD CONSTRAINT `kpr1` FOREIGN KEY (`kode_prestasi`) REFERENCES `tb_prestasi` (`kode_prestasi`),
  ADD CONSTRAINT `kps1` FOREIGN KEY (`no_kps`) REFERENCES `tb_kps` (`no_kps`),
  ADD CONSTRAINT `krg1` FOREIGN KEY (`kode_regis`) REFERENCES `tb_regis` (`kode_regis`),
  ADD CONSTRAINT `npu1` FOREIGN KEY (`nomor_peserta_ujian`) REFERENCES `tb_ujian` (`nomor_peserta_ujian`);

--
-- Ketidakleluasaan untuk tabel `tb_ibu`
--
ALTER TABLE `tb_ibu`
  ADD CONSTRAINT `ibu1` FOREIGN KEY (`kode_pendidikan`) REFERENCES `tb_pendidikan` (`kode_pendidikan`),
  ADD CONSTRAINT `ibu2` FOREIGN KEY (`kode_pekerjaan`) REFERENCES `tb_pekerjaan` (`kode_pekerjaan`),
  ADD CONSTRAINT `ibu3` FOREIGN KEY (`kode_penghasilan`) REFERENCES `tbl_penghasilan` (`kode_penghasilan`),
  ADD CONSTRAINT `ibu4` FOREIGN KEY (`kode_kebutuhan_khusus`) REFERENCES `tb_kebutuhan_khusus` (`kode_kebutuhan_khusus`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
